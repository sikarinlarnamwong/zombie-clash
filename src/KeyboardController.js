var KeyboardController = cc.Class.extend({
    ctor: function( movementController ) {
        this._movementController = movementController;
        this._isLeftButton = false;
        this._isRightButton = false;
    },
    
    onKeyDown: function ( e ) {
        if ( e == KeyboardController.KEY_SET.LEFT ) {
            this.onLeftDown();
        } else if ( e == KeyboardController.KEY_SET.RIGHT ) {
            this.onRightDown();
        }
    },
    
    onKeyUp: function ( e ) {
        if ( e == KeyboardController.KEY_SET.LEFT ) {
            this.onLeftUp();
        } else if ( e == KeyboardController.KEY_SET.RIGHT ) {
            this.onRightUp();
        }
    },
    
    onLeftDown: function () {
        this._isLeftButton = true;
        this._movementController.setMovementState( ConstantMovementController.STATE.LEFT );
    },
    
    onRightDown: function () {
        this._isRightButton = true;
        this._movementController.setMovementState( ConstantMovementController.STATE.RIGHT );
    },
    
    onLeftUp: function () {
        this._isLeftButton = false;
        if( this._isRightButton ) {
            this._movementController.setMovementState( ConstantMovementController.STATE.RIGHT );
        } else {
            this._movementController.setMovementState( ConstantMovementController.STATE.STAY );
        }
    },
    
    onRightUp: function () {
        this._isRightButton = false;
        if( this._isLeftButton ) {
            this._movementController.setMovementState( ConstantMovementController.STATE.LEFT );
        } else {
            this._movementController.setMovementState( ConstantMovementController.STATE.STAY );
        }
    }
});

KeyboardController.KEY_SET = {
    LEFT : cc.KEY.left,
    RIGHT : cc.KEY.right
}