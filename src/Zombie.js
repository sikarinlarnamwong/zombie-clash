var Zombie = cc.Sprite.extend({
    ctor: function( movementController ) {
        this._super();
        
        this.initWithSpriteFrame(  cc.SpriteFrame.create( "res/images/character0001.png", cc.rect(0,0,90,360) ) );
        this.runAction( cc.RepeatForever.create( cc.Sequence.create( cc.DelayTime.create( 0.1 ), cc.CallFunc.create( this.flip, this ) ) ) );
        //this.characterSprite = this.initSprite();
        //this.addChild( this.characterSprite );
        //this.characterSprite.setPosition( 0,0 );
        //this.runAction( this.initAction() );

        this._movementController;
        this._gameStage;
        this._currentPosition = 0;
        this.scheduleUpdate();
        this.setMovementController( movementController );
        this.setScaleX(0.3);
        this.setScaleY(0.3);
    },

    flip : function(){
        this.setScaleX( this.getScaleX() * -1 );
    },

    initAction: function () {
        var array = [];
        array.push( cc.SpriteFrame.create( "res/images/character0001.png", cc.rect(0,0,90,360) ) );
        array.push( cc.SpriteFrame.create( "res/images/character0002.png", cc.rect(0,0,90,360) ) );
        var animation = new cc.Animation();
        animation.setDelayPerUnit( 0.1 );
        animation.setFrames( array );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
    
    update: function (  ) {
        if( this._movementController ) {
            this._movementController.update();
        }
    },

    setStage: function ( stage ) {
        this._gameStage = stage;
        this.setCurrentPosition( this._currentPosition );
    },
    
    getCurrentPosition: function () {
        return this._currentPosition;
    },
    
    setMovementController: function ( movementController ) {
        this._movementController = movementController
        movementController.setCharacter( this );
    },
    
    setCurrentPosition: function ( number ) { /* -1...1 */
    	number = Math.max(-1,Math.min(number,1));
        this._currentPosition = number;
        if ( this._gameStage ) {
            this.setPosition( cc.p( ( ( number + 1 ) / 2 ) * this._gameStage.WIDTH , this.getPositionY() ) );
        }
    },
    
    onCollisionObstacle: function ( obstacle ) {
        this.die();
    },
    
    die: function () {
        cc.AudioEngine.getInstance().playEffect( "res/sounds/DieFx.wav" );
        this._gameStage.setGameState( GameStage.STATE.END );
        this._movementController.setEnable( false );
    },
    
    setControllerEnable: function ( bool ) {
        this._movementController.setEnable( true );
    }
});
