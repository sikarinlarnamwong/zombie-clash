var SingleTubeObstacle = cc.Sprite.extend({
    ctor: function () {
        this._super();
        this.initWithFile( 'res/images/tube.png' );
        this.setAnchorPoint( cc.p( 0.5, 0 ) )
        this.setRotation( -90 );
        this.setScale( 3 );
    },
    
    isCollisionCharacter: function ( zombie ) {
        return cc.rectIntersectsRect( this.getBoundingBoxToWorld(), zombie.getBoundingBoxToWorld() );
    },
    
    randomObstaclePattern: function () {
        console.warn('Useless method have been used.');
    }
});

