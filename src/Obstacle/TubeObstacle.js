var TubeObstacle = ObstacleNode.extend({
    ctor: function ( randomWidth, gapWidth ) {
        ObstacleNode.prototype.ctor.call( this );

        this.sinValue=0;
        this._gapWidth = gapWidth ? gapWidth : 300;
        this._randomWidth = randomWidth;
        var secondObstacle = new SingleTubeObstacle();
        secondObstacle.setRotation( secondObstacle.getRotation() + 180 );
        this.addObstacle( new SingleTubeObstacle(), cc.p( 0, 0 ) );
        this.addObstacle( secondObstacle, cc.p( this._gapWidth, 0 ) );
        this.resetScore();
        this.scheduleUpdate();
        
    },
    update :function(){
        if(gameScore>=30){
            activeTubeMove();
        }
    },
    activeTubeMove : function(){
        this.sinValue += 2*Math.PI / 5000-(0.005);
        this.sinValue %= 2*Math.PI;
        this.setPositionX( Math.abs(Math.sin(this.sinValue)) * ( this._randomWidth - this._gapWidth ) );
    },
    resetObstacle: function () {
    	this.randomObstaclePattern();
    	this.resetScore();
        this.sinValue=0;
        this.scheduleUpdate();
    },
    
    randomObstaclePattern: function () {
        if(gameScore<20){
             this.setPositionX( Math.random() * ( this._randomWidth - this._gapWidth ) );
        }
        else if(gameScore<40){
            if(isFlip){
                this.setPositionX( 0.9 * ( this._randomWidth - this._gapWidth ) );
            }
            else{
                this.setPositionX( 0.1 * ( this._randomWidth - this._gapWidth ) );
            }
            isFlip = !isFlip;
        }
        else{
           cosNumber += 2*Math.PI / 10;
            cosNumber %= 2*Math.PI
            this.setPositionX( Math.abs(Math.cos( cosNumber )) * ( this._randomWidth - this._gapWidth ) );
        }
        
    },
    
	resetScore: function () {
		this._score = 1;	
	},
    
    isPassed: function ( character ) {
        return character.getPositionY() > this.getPositionY(); 
    },
    
    pullScore: function () {
    	var i = this._score;
    	this._score = 0;
    	return i;
    }
});

var cosNumber = 0;
var isFlip=true;