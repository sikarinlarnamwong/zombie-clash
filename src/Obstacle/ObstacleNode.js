var ObstacleNode = cc.Node.extend({
    ctor: function () {
        this._super();
        this._obstacles = []
    },
    
    // interface Obstacle
    isCollisionCharacter: function ( zombie ) {
        for ( var i = 0 ; i < this._obstacles.length ; i++ ) {
            if ( this._obstacles[i].isCollisionCharacter( zombie ) ) return true;
        }
        return false;
    },
    
    // interface Obstacle
    resetObstacle: function () {
    	console.error( 'ObstacleNode cannot resetObstacle themself, Override this.' );
    },
    
    // interface Obstacle 
    isPassed: function ( character ) {
        console.error( 'ObstacleNode cannot isPassed themself, Override this.' );
    },
    
    // interface Obstacle ( not node behavior )
    randomObstaclePattern: function () {
        console.error( 'ObstacleNode cannot randomPattern themself, Override this.' );
    },
    
    // interface Obstacle 
    pullScore: function () {
        console.error( 'ObstacleNode cannot pullScore themself, Override this.' );
    },
    
    addObstacle: function ( obstacle, positionPoint ) {
        this._obstacles.push( obstacle );
        this.addChild( obstacle );
        obstacle.setPosition( positionPoint );
    }
});
