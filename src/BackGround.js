var BackGround = cc.Sprite.extend({
	ctor: function () {
		cc.Sprite.prototype.ctor.call( this );
		this.bgHeight = 1850;
		this.firstBg = this.createBg();
		this.secondBg = this.createBg();
		this.addChild( this.firstBg );
		this.addChild( this.secondBg );
		this.secondBg.setPositionY( this.bgHeight );
		
		
	},

	createBg: function () {
		var sp = new cc.Sprite();
		sp.initWithFile( BackGround.file );
		sp.setAnchorPoint( 0,0 );

		sp.setPosition( 0,0 );
		return sp;
	},

	move: function ( amount ) {
		this.firstBg.setPositionY( this.firstBg.getPositionY() - amount );
		this.secondBg.setPositionY( this.secondBg.getPositionY() - amount );
		if ( this.secondBg.getPositionY() < 0 ) {
			this.firstBg.setPositionY( this.firstBg.getPositionY() + this.firstBg.getBoundingBox().height );
			this.secondBg.setPositionY( this.secondBg.getPositionY() + this.secondBg.getBoundingBox().height );
		}
	}
});

BackGround.file = "res/images/bg.jpg";
BackGround.height = 1850;
var a;