var ScoreLayer = cc.LayerColor.extend({
    init: function( gameStage ) {
        this._super( new cc.Color4B( 0, 0, 0, 100 ) );
        this._gameStage = gameStage;

        var sprite = new cc.Sprite();
        sprite.initWithFile( "res/images/restart.png" );
        var menuItem = cc.MenuItemSprite.create( sprite, null, null, this.onRestartClick , this);
        var menu = cc.Menu.create( menuItem );
        this.addChild( menu, 1 );

        this.setVisible( false );
        this._gameStage.setGameEndCallBack( this.onGameEnd, this );
    },
    
    onGameEnd: function () {
        this.showScore();
    },
    
    showScore: function () {
        this.setVisible( true );
    },
    
    onRestartClick: function () {
        this._gameStage.init();
        this._gameStage.setGameState( GameStage.STATE.PLAY );
        this.setVisible( false );
    }
});