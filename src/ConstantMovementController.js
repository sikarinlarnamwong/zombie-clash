var ConstantMovementController = cc.Class.extend({
    ctor: function( speed  ) {
        this._speed = speed;
        this._character;
        this._enable = true;
        this._movementState = ConstantMovementController.STATE.STAY;
        
    },
    
    setCharacter: function ( character ) {
        this._character = character;
    },
    
    update: function () {
        if ( this._movementState == ConstantMovementController.STATE.LEFT ) {
            this._character.setCurrentPosition( this._character.getCurrentPosition() - this._speed );
        } else if ( this._movementState == ConstantMovementController.STATE.RIGHT ) {
            this._character.setCurrentPosition( this._character.getCurrentPosition() + this._speed );
        } 
    },
    
    setEnable: function ( bool ) {
        this._enable = bool;
        if ( !bool ) this._movementState = ConstantMovementController.STATE.STAY;
    },
    
    setMovementState: function ( state ) {
        if ( this._enable ) this._movementState = state;
    }
});

ConstantMovementController.STATE = {
    LEFT : -1,
    RIGHT : 1,
    STAY : 0
}