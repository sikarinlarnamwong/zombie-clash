var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color4B( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.setKeyboardEnabled( true );
        
        cc.AudioEngine.getInstance().setMusicVolume( 0.5 );
        
        this.scoreLabel = cc.LabelTTF.create('0',  'Times New Roman', 100, cc.size(100,100), cc.TEXT_ALIGNMENT_CENTER );
        this.scoreLabel.setPosition ( cc.p( winSize.width / 2, winSize.height / 5 * 4 ) );
        
        this.movementController = new ConstantMovementController( 0.055 );
        this.keyboardController = new KeyboardController( this.movementController );
        this.character = new Zombie( this.movementController );
        this.gameStage = new GameStage( cc.size( 1080, 1920 ), this.character, 8 , [ new TubeObstacle( 1080 ),new TubeObstacle( 1080 ),new TubeObstacle( 1080 ),new TubeObstacle( 1080 ),new TubeObstacle( 1080 ),new TubeObstacle( 1080 ),new TubeObstacle( 1080 ) ]);
        this.gameStage.init();
        this.gameStage.setScoreCallBack( this.onScoreCB, this );
        this.addChild( this.gameStage );
        this.addChild( this.scoreLabel );
        
    },
    
    onScoreCB: function ( score ) {
    	this.scoreLabel.setString( score + '' );
    },
    
    getGameStage: function () {
        return this.gameStage;
    },
    
    onKeyDown:  function ( e ) {
        this.keyboardController.onKeyDown( e );
    },
    
    onKeyUp: function ( e ) {
        this.keyboardController.onKeyUp( e );
    },
    
});

var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        this.layer = new GameLayer();
        this.scoreLayer = new ScoreLayer();
        this.layer.init();
        this.scoreLayer.init( this.layer.getGameStage() );
        this.addChild( this.layer );
        this.addChild( this.scoreLayer );
    }
});