var gameScore = 0;

var GameStage = cc.Node.extend({
    ctor: function( size, character, speed, obstacles ) {
        this._super();
        this._character;
        this._gameState;
        this._obstacles = [];
        this._obstaclePool;
        this._initSpeed = speed;
        this._backGroundSprite = new BackGround();
		this.WIDTH = size.width;
        this.HEIGHT = size.height;
        this.OBSTACLE_FIRST_INSTANTIATE_Y = this.HEIGHT + 200;
        this.OBSTACLE_INSTANTIATE_Y = this.HEIGHT - 200;//( when last obstacle down to this position will instantiate new obstacle)
        this.OBSTACLE_GARBAGE_Y = -200;
        this.OBSTACLE_SPACE_Y = 550;
        this.CHARACTER_Y = this.HEIGHT / 10;
        this.scheduleUpdate();

        this.addChild( this._backGroundSprite );
        this._backGroundSprite.setPosition( cc.p(0,0))
        this.setObstaclesToPool( obstacles );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.setCharacter( character );
    },
    
    init: function (  ) {
        this._character.setControllerEnable ( true );
       	this.setSpeed( this._initSpeed );
       	this.setScore( 0 );
        this.setGameState( GameStage.STATE.PLAY );
        this.collectAllObstaclesToPool();
        if ( this._obstaclePool.length == 0 ) throw 'No Obstracle Found';
        this.executeObstacleFromPool( this.OBSTACLE_FIRST_INSTANTIATE_Y );
    },

    getScore: function () {
    	return gameScore;
    },
    
    setScore: function ( score ) {
    	if( score != gameScore ) {
			gameScore = score;
    		this.refreshSpeed();
    		this.callScoreCb( gameScore ); //dispatch score
    	}
    },
    
    refreshSpeed: function () {
    	this.setSpeed ( this.getSpeed() + this.getScore() * 0.02 );
    },
    
    setCharacter: function ( zombie ) {
        this._character = zombie;
        this._character.setStage( this );
        this._character.setPositionY( this.CHARACTER_Y );
        this.addChild( this._character );
    },
    
    update: function () {
        if ( this._gameState ) {
            this._gameState.exec( this );
        }
    },
    
    setSpeed: function ( speed ) {
        this._speed = speed;
    },
    
    getSpeed: function () {
        return this._speed;
    },
    
    setPlayerController: function ( controller ) {
        controller.setCharacter( this._character );
    },
    
    move: function () {
        this.moveObstacles();
        this._backGroundSprite.move( this._speed );
        if( this.getWorseObstacle().getPositionY() <= this.OBSTACLE_GARBAGE_Y ) {
            this.collectObstacleToPool( this.getWorseObstacle() );
        }
        if( this.getTopObstacle().getPositionY() <= this.OBSTACLE_INSTANTIATE_Y ) {
            this.executeObstacleFromPool( this.getTopObstacle().getPositionY() + this.OBSTACLE_SPACE_Y );
        }
    },
    
    moveObstacles: function () {
        var obst;
        for ( var i = 0 ; i < this._obstacles.length ; i++ ) {
            obst = this._obstacles[i];
            obst.setPositionY( obst.getPositionY() - this._speed );
            if ( obst.isCollisionCharacter( this._character ) ) {
                this._character.onCollisionObstacle( obst );
            }
            if ( obst.isPassed( this._character ) ) {
            	this.setScore( this.getScore() + obst.pullScore() );
            }
        }
    },
    
    getTopObstacle: function () {
        if ( this._obstacles.length == 0 ) return null;
        return this._obstacles[ this._obstacles.length - 1 ];
    },
    
    getWorseObstacle: function () {
        if ( this._obstacles.length == 0 ) return null;
        return this._obstacles[ 0 ];
    },
    
    setGameState: function ( state ) {
        if ( this._gameState )
            this._gameState.perf( this );
        this._gameState = state;
        if ( this._gameState )
            this._gameState.init( this );
    },
    
    setObstaclesToPool: function ( obstacles ) {
        this._obstaclePool = obstacles;
    },
    
    executeObstacleFromPool: function ( yPosition ) {
        var obst = this._obstaclePool.shift();
        this._obstacles.push( obst );
        this.addChild( obst );
        obst.setPositionY( yPosition );
        obst.resetObstacle();
        return obst;
    },
    
    collectObstacleToPool: function ( obstacle ) {
        var indexof = this._obstacles.indexOf( obstacle );
        if ( indexof == -1 ) return null;
        this._obstacles.splice( indexof, 1 );
        this._obstaclePool.push( obstacle );
        this.removeChild( obstacle );
        return obstacle;
    },
    
    collectAllObstaclesToPool: function () {
        while ( this._obstacles.length != 0 )
            this.collectObstacleToPool( this._obstacles[0] );
    },
    
    callScoreCb: function ( n ) {
    	if ( this.SCORE_CB ) {
    		this.SCORE_CB.call( this.SCORE_CB_TARGET, n );
    	}
    },
    
    callEndGameCb: function () {
    	if ( this.GAMEEND_CB ) 
    		this.GAMEEND_CB.call( this.GAMEEND_CB_TARGET );
    },
    
    setGameEndCallBack: function ( cb, target ) {
        this.GAMEEND_CB = cb;
        this.GAMEEND_CB_TARGET = target;
    },
    
    setScoreCallBack: function ( cb, target ) {
    	this.SCORE_CB = cb;
    	this.SCORE_CB_TARGET = target;
    }
    
});

GameStage.STATE = {
    PLAY: {
        init: function ( stage ) { this.music = cc.AudioEngine.getInstance().playMusic( "res/sounds/music.mp3", true ) },
        exec: function ( stage ) { stage.move(); },
        perf: function ( stage ) {}
    },
    END: {
        init: function ( stage ) {
            cc.AudioEngine.getInstance().stopMusic( this.music );
            stage.callEndGameCb();
        },
        exec: function ( stage ) {},
        perf: function ( stage ) {}
    }
}